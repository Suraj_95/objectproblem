function defaults(data, defaultData) {
  for (let i in defaultData) {
    if (!(i in data)) {
      data[i] = defaultData[i];
    }
  }
  return data;
}
module.exports = defaults;
