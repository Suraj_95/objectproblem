function invert(data) {
  const keys = Object.keys(data);
  const values = Object.values(data);
  const res = {};
  for (let i = 0; i < keys.length; i++) {
    res[values[i]] = keys[i];
  }
  return res;
}
module.exports = invert;
