function mapObject(data, callBack) {
  const res = {};
  for (let i in data) {
    res[i] = callBack(data[i]);
  }
  return res;
}
module.exports = mapObject;
