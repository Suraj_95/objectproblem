const data = require("../data");
const mapObject = require("../mapObject");

function callBack(value) {
  return value + 5;
}

const res = mapObject(data, callBack);
console.log(res);
